# VuePress Gitlab Pages

Use [VuePress](https://vuepress.vuejs.org/) to built a static website and deploy on [Gitlab Pages](https://about.gitlab.com/features/pages/).

## Start

Make sure that you have NodeJS and NPM or Yarn installed.

Start by forking or downloading [this repository](https://gitlab.com/scriptex/gitlab-pages-vuepress). If downloaded, extract the contents of the archive in a folder and navigate to that folder in your preferred terminal. Then:

## Install

```sh
npm install
```

or

```sh
yarn
```

## Develop

```sh
npm run start
```

or

```sh
yarn start
```

## Build

```sh
npm run start
```

or

```sh
yarn start
```

## Details

The `README.md` file in the root server the purpose of `index.html` file.

If you wish to create new pages, you can do so by adding new `.md` files in the root.

If you wish to use a more complicated files/folders structure, make sure that you read and follow the reference in https://vuepress.vuejs.org/

VuePress builds your static website in the `/public` folder which is set to be used by Gitlab CI to deploy on Gitlab Pages.

**Important**

You can control the base dir of your site in the `.vuepress/config.js` file's `base` property.

## Demo

See this page server via Gitlab Pages on https://three11.gitlab.io/gitlab-pages-vuepress/

## Bonus: Theming

Vuepress uses [Stylus](http://stylus-lang.com/). It comes with default theme which can be easily overwritten.
Just place your styles in the `override.styl` file in the `.vuepress` folder.

Here are the **default theme** colors:

```stylus
$accentColor = #3eaf7c
$textColor = #2c3e50
$borderColor = #eaecef
$codeBgColor = #282c34
```

Here are the colors and settings for a **Material Light** theme:

```stylus
$accentColor = #009688
$textColor = #212121
$borderColor = #bdbdbd
$codeBgColor = #333
$bgColor = #fff

html,
body,
.navbar,
.sidebar,
.theme-container .navbar,
.theme-container .sidebar
	background-color $bgColor

.theme-container .search-box input
	color $codeBgColor

.theme-container .search-box .suggestion a
	color $accentColor

.theme-container .content code
	color $bgColor
	background-color $codeBgColor
```

Here are the color for a **Material Dark** theme (used in my personal website):

```stylus
$accentColor = #ef4c23
$textColor = #fff
$borderColor = #bdbdbd
$codeBgColor = #000
$bgColor = #263238

html,
body,
.navbar,
.sidebar,
.theme-container .navbar,
.theme-container .sidebar
	background-color $bgColor

.theme-container .search-box input
	color: $codeBgColor

.theme-container .search-box .suggestion a
	color: $accentColor

.theme-container .content code
	background-color $codeBgColor
```

## LICENSE

MIT
